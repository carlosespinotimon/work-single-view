# Work Single View

[![codecov](https://codecov.io/gl/carlosespinotimon/work-single-view/branch/master/graph/badge.svg?token=ZLM518PCRY)](https://codecov.io/gl/carlosespinotimon/work-single-view/blanch/master) 
![pipeline status](https://gitlab.com/carlosespinotimon/work-single-view/badges/master/pipeline.svg)](https://gitlab.com/carlosespinotimon/work-single-view/-/commits/master)

This is an example of an API for musical works. This API implements a CRUD for musical works and some functions that allow users to upload information about musical works thought a csv file or download a csv file for a specific work. The technologic stack consists in :
- Server: developed in Python 3.8.5 with the Flask framework
- Database: a Postgre 12.4 database


##### Table of Contents
1. [Getting Started](#1-getting-started)
2. [Working in the project](#2-working-in-the-project)
3. [Deployment](#3-deployment)
4. [Actual state of the project](#4-actual-state-of-the-project)


## 1. Getting Started
-----------------------

### 1.1. __Prerequisites__

This project is configured to use the [Visual Studio Code Remote Containers extension](https://code.visualstudio.com/docs/remote/containers)
You need to have installed:
- docker
- docker-compose

#### __How to install Docker__

Here are some links to install Docker in [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/), [Mac](https://docs.docker.com/docker-for-mac/install/) and [Windows](https://docs.docker.com/docker-for-windows/install/).

#### __How to install docker-compose__

In this [link](https://docs.docker.com/compose/install/) there is information about how to install docker-compose in the Ubuntu, Mac and Windows OS.


### 1.2. __Set the environment variables__

To facilitate the configuration of the app for development and production (using [12-factor](https://12factor.net/) principles), the server takes some environment variables from and .env file.

The file should look something like this:
```
# DEFAULT VARIABLES
# ---------------------------------------------------------------------
# Server variables for development
FLASK_APP=api/main.py
FLASK_ENV=development
CONFIG_MODE='config.Dev'
PYTHONUNBUFFERED=1
SQLALCHEMY_DATABASE_URI=postgresql://postgres:supercomplicatedpassword@database:5432/wsv_db
REDIS='redis://redis'

# Production database URI
DATABASE_URI=YOUR_MYSQL_URI

```

This file is in the gitignore, so no credentials are going to be uploaded to the repository.


The `DATABASE_URI` is just for production, the config.Dev configuration configures by default the URI to the local database declared in the docker-compose, so you can leave it like this in the example.

### 1.3. __Run the server__

To run the project you just need to open Visual Studio Code (with the Remote Container extension installed) and `Remote-Containers: Open Folder in Container...` command from the Command Palette (F1) or quick actions Status bar item, and select the project folder you would like to set up the container for.

The first time this will be slow as it needs to build the image. You will see how a new window of Visual Studio opens with a shell that is runned by the user `vscode@id_of_the_container`.

From there you can run start the server by running `flask run`.

**NOTE:** The first time you start the server (after open folder in container) you have to open the url from the VS shell by ALT+clicking in the url shown in the terminal:
`Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)`

After that, you can open it by typing in any browser.

### 1.4. __Upgrade database__

This project uses [Flask SQL Alchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/) as ORM and [flask-Migrate](https://flask-migrate.readthedocs.io/en/latest/) to control the migrations in the database. The first time you run the server you have to upgrade your database. 

To do so, you can just execute:

`flask db upgrade`

### 1.5. __Running the tests__

The test are run in a test database, to run them you can just execute (from the `api` directory):

`python api/tests/tests.py`

### 1.6. __Create a User to interact with the API__

To access the Customer endpoints you need to have a register user. There is a Python script in the `api` directory called `create_user.py`, that recieves the email and password as params, it encrypts the password and insert the record in the database. You can execute the script (from the `api` directory):

`python create_user.py YOUR_EMAIL YOUR_PASSWORD`

After that you can log in the system.

## 2. Working in the project
------------------------------

This project is build in a dockerized environment and it has some peculiarities. Everything the developer needs is inside the backend container. 

As you will see, there is a githook configured and all the actions the githooks does are run in the container.


### 2.1. __GitHooks__

In this project I have configured a githook that is run before each commit to ensure that the code to be commited passes all the test, has no codestyle errors (regarding the PEP8) and in the end, if everything has gone ok, it generates the API documentation with Sphinx. After all this, the commit is done.

This githook needs to be configured the first time and it's done by running `git config core.hooksPath .githooks`.


### 2.2. __Debug the code__

As this project is configured to use the Visual Studio Code Remote Container Extension, debugging is already configured, you just have to set a breakpoint and press `F5`, the debugger will start and the server will start in port `5001`.

**NOTE:** The first time you start the server (after open folder in container) you have to open the url from the VS shell by ALT+clicking in the url shown in the terminal:
`Running on http://127.0.0.1:5001/ (Press CTRL+C to quit)`

After that, you can open it by typing in any browser.


### 2.3. __Access Postgre Database__

This project starts a Postgres DB when you open the project with the extension. This database is run inside a container.

If you want to access the database you can run:

`docker ps` 

To list all the containers running. Then, you can access by running:

`docker exec -it CONTAINER_ID bash`

Once you are in, you can connect to the database:

`PGPASSWORD=supercomplicatedpassword psql -h database -d wsv_db -U postgres`

And make some queries like:

`SELECT * FROM works;`


## 2.4. __Documentation__

As you can see in the GitHooks section, this proyect is configured to automatically generate the documentation for the API with Sphinx before each commit. The generated html code is in `api/docs/_build/html/` you can open the `index.html` and navigate through the documentation.

If you want to see the documentation without doing any commit you can run (from the `docs` directory):

`sphinx-apidoc -o . ../api/ --ext-autodoc && make clean && make html`

This will generate the documentation in `api/docs/_build/html`


## 2.5. __Codestyle__

The githook that we have talked before check for codestyle errors in the code. The command run is:
`find ./ -not -path "./docs*" -not -path "./migrations*" -iname "*.py" | xargs pycodestyle --quiet`

However, if you want to see a detailed output you can run:
`find ./ -not -path "./docs*" -not -path "./migrations*" -iname "*.py" | xargs pycodestyle --verbose`

If you want to fix the errors you can use this command (from you system, `not from the terminal in the VS Code` as it needs Docker):

`docker run -v $(pwd)/api/:/tmp/code webpp/codestyle -i /tmp/code -x '/tmp/code/docs' '/tmp/code migrations'`

This will modify your files and fix the Codestyle errors.


## 3. Deployment
------------------


### 3.1. __Automated deployment__

This repository has a GitLab pipeline configured, that run the tests, if tests are passed, it deploys the code to App Engine Flex.

### 3.2. __Manual deployment__

This server is prepared to be deployed in Google App Engine Flexible, if you wish to deploy it manually, you need to configure the `app.yaml` file and generate the latest `requirements.txt`.

#### 3.2.1 __Configure app.yaml__

In the repository there is an `app.yaml` example, in this file you have to change the environment variables for the real ones. For security, to avoid uploading credentials to the repository, you should create a copy of the file and call it `real_app.yaml` which is already in the .gitignore, put the real environment variables there and deploy it with this file.

#### 3.2.2. __Generate requirements.txt__

To generate the latest `requirements.txt` by running:
`pip freeze > requiremets.txt`

It is also needed the gunicorn and setuptools modules:

`echo "gunicorn==20.0.4" >> requirements.txt`

`echo "setuptools==40.3.0" >> requirements.txt`


#### 3.2.3. __Deploy it__

Once you have it, you can deploy the app by running: `gcloud app deploy real_app.yaml` in the `api` directory (outside the container). Beware that you have to have [initiated the cloud SDK](https://cloud.google.com/sdk/docs/initializing "Initializing Cloud SDK")

## 4. Actual state of the project
---------------------------------

### 4.1. TaskQueue
This API has an endpoint that allow users to upload works through a csv file. This action, regarding the size of the file, can take a long time, so ideally should be ran asynchronously in the context of the request. A Task queu provide a convenient solution for this problem as is configures a worker process that runs independently of the application and executes the task.

Right now this is implemented in the branch `feature/task-queue`, but it has not being merged to master as all the tests related with the csv upload are broken. Due to the lack of time, this had to be in my own task queue :-)

