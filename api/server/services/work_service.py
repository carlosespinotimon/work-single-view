from datetime import datetime
from flask import jsonify

from server import db
from ..models.work_model import Work, WorkSchema, CSV_HEADER_FORMAT, CSVWork

work_schema = WorkSchema()


def get_a_work(iswc):
    work = Work.query.filter_by(iswc=iswc).first()
    if work and work.is_deleted is False:
        response = work_schema.dump(work), 200
    else:
        response = jsonify('Work not found'), 404
    return response


def all_works():
    works = Work.query.filter_by(is_deleted=False)
    return jsonify([work_schema.dump(work) for work in works]), 200


def create_work(data):
    iswc = data.get('iswc')
    if iswc.strip() == "":
        return jsonify('Work iswc must not be empty'), 400
    work = Work.query.filter_by(iswc=iswc).first()
    if not work:
        work = Work(
            iswc=data.get('iswc'),
            title=data.get('title'),
            contributors=data.get('contributors'),
            created_by=data.get('user_id'),
            last_modified_by=data.get('user_id'),
            created_at=datetime.now(),
            last_modified_at=datetime.now()
        )
        if data.get('csv_loading'):
            work.source = data.get("source")
        else:
            work.source = f'{data.get("source")}--{data.get("id")}'
        _save_work(work)
        response = work_schema.dump(work), 201
    else:
        response = jsonify('Work already exists'), 409
    return response


def update_work(data):
    work = Work.query.filter_by(iswc=data.get('iswc')).first()
    if work and not work.is_deleted:
        fields_to_update = ['title', 'contributors']
        for field in fields_to_update:
            if field in data:
                _process_field(work, data, field)
        _process_source(work, data)
        work.last_modified_by = data.get('user_id')
        work.last_modified_at = datetime.now()
        _save_work(work)
        response = jsonify('Work sucessfully updated'), 200
    elif work and work.is_deleted:
        response = jsonify('Work has being deleted'), 409
    else:
        response = jsonify('Work not found'), 404
    return response


def delete_a_work(iswc, user_id):
    work = Work.query.filter_by(iswc=iswc).first()
    if work:
        work.last_modified_by = user_id
        work.last_modified_at = datetime.now()
        work.is_deleted = True
        _save_work(work)
        response = jsonify('Work deleted'), 200
    else:
        response = jsonify('Work not found'), 404
    return response


def activate_a_work(iswc, user_id):
    work = Work.query.filter_by(iswc=iswc).first()
    if work:
        work.last_modified_by = user_id
        work.last_modified_at = datetime.now()
        work.is_deleted = False
        _save_work(work)
        response = jsonify('Work activated'), 200
    else:
        response = jsonify('Work not found'), 404
    return response


def process_csv(data, user_id):
    header = data.pop(0)
    if not header == CSV_HEADER_FORMAT:
        return jsonify('Unprocessable Entity, wrong header'), 422

    csv_works = [CSVWork._make(work) for work in data]
    csv_works = [work for work in csv_works if work.iswc != ""]

    aggregated = _aggregate_content_of_same_work(csv_works)
    processed = [_remove_dup(work, iswc) for iswc, work in aggregated.items()]

    for work in processed:
        work_stored = Work.query.filter_by(iswc=work['iswc']).first()
        work['user_id'] = user_id
        work['csv_loading'] = True
        if not work_stored:
            create_work(work)
        else:
            update_work(work)

    return jsonify('All works have being processed'), 200


def get_csv_work(iswc):
    work = Work.query.filter_by(iswc=iswc).first()
    if work and work.is_deleted is False:
        dumped_work = work_schema.dump(work)
        header = CSV_HEADER_FORMAT
        header.remove("id")
        work = [dumped_work[k] for k in header]
        return [header, work]


def _save_work(work):
    db.session.add(work)
    db.session.commit()


def _aggregate_content_of_same_work(works):
    processed = {}
    for work in works:
        if processed.get(work.iswc):
            processed[work.iswc]['title'].append(work.title)
            processed[work.iswc]['contributors'].extend(
                work.contributors.split("|"))
            processed[work.iswc]['source'].append(
                f'{work.source}--{work.id}')
        else:
            processed[work.iswc] = {
                'title': [work.title],
                'contributors': work.contributors.split("|"),
                'source': [f'{work.source}--{work.id}'],
                'iswc': work.iswc
            }
    return processed


def _remove_dup(work, iswc):
    new_work = {}
    for k, v in work.items():
        new_work[k] = "|".join(list(set(v)))
    new_work['iswc'] = iswc
    return new_work


def _process_field(work, data, field):
    stored_field = getattr(work, field).split("|")
    new_field = data[field]
    stored_field.extend(new_field.split("|"))
    field_to_store = "|".join(
        [el for el in list(set(stored_field)) if el.strip() != ""]
    )
    setattr(work, field, field_to_store)


def _process_source(work, data):
    if data.get('csv_loading'):
        _process_field(work, data, "source")
    else:
        processed_source = work.source.split("|")
        processed_source.append(f'{data["source"]}--{data["id"]}')
        work.source = "|".join(list(set(processed_source)))
