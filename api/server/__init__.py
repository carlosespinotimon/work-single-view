from flask import Flask, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow


db = SQLAlchemy()
migrate = Migrate()


def create_app(app_config):
    app = Flask(__name__)
    app.config.from_object(app_config)

    CORS(app)

    db.init_app(app)
    migrate.init_app(app, db)

    from .models.user_model import User

    # Import blueprints
    from .controllers.work_controller import works
    app.register_blueprint(works)
    from .controllers.authentication_controller import authentication
    app.register_blueprint(authentication)

    @app.route('/')
    def server_running():
        return jsonify('The server is running!!!')

    return app
