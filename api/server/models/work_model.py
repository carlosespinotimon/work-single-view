import collections
from datetime import datetime
from server import db
from marshmallow import Schema, fields

CSV_HEADER_FORMAT = ['title', 'contributors', 'iswc', 'source', 'id']
CSVWork = collections.namedtuple(
    'CSVWork', 'title contributors iswc source id')


class Work(db.Model):
    __tablename__ = 'works'
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True)
    iswc = db.Column(db.String(36), index=True, unique=True)
    title = db.Column(db.String(128))
    contributors = db.Column(db.String(128))
    source = db.Column(db.String(128))
    created_by = db.Column(
        db.Integer,
        db.ForeignKey('users.id'),
        nullable=False)
    last_modified_by = db.Column(
        db.Integer,
        db.ForeignKey('users.id'),
        nullable=False)
    created_at = db.Column(
        db.DateTime,
        index=True,
        nullable=False,
        default=datetime.utcnow)
    last_modified_at = db.Column(
        db.DateTime,
        index=True,
        nullable=False,
        default=datetime.utcnow)
    is_deleted = db.Column(
        db.Boolean,
        index=True,
        nullable=False,
        default=False)


class WorkSchema(Schema):
    iswc = fields.Str(required=True)
    title = fields.Str(required=True)
    contributors = fields.Str(required=True)
    source = fields.Str(required=True)
    id = fields.Str(required=True)


class CreateWorkSchema(Schema):
    iswc = fields.Str(required=True)
    title = fields.Str(required=True)
    contributors = fields.Str(required=True)
    source = fields.Str(required=True)
    id = fields.Str(required=True)


class UpdateWorkSchema(Schema):
    title = fields.Str(required=True)
    contributors = fields.Str(required=True)
    source = fields.Str(required=True)
    id = fields.Str(required=True)
