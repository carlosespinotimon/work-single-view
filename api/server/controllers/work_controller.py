import io
import csv

from flask import Blueprint, request, jsonify, make_response
from flask_cors import CORS

from ..services.work_service import (
    all_works,
    create_work,
    get_a_work,
    update_work,
    delete_a_work,
    activate_a_work,
    process_csv,
    get_csv_work
)
from ..models.work_model import CreateWorkSchema, UpdateWorkSchema
from .decorators import validate_schema, protected_endpoint
from .token_managment import get_user_id_from_token

works = Blueprint('works', __name__, url_prefix='/works')
CORS(works, max_age=30 * 86400)


@works.route('/')
def get_all_works():
    """
    .. http:get:: /works/

    Function that returns all the stored works.

    :returns: The Works.
    :rtype: list.
    """
    return all_works()


@works.route('/<string:iswc>')
def get_work(iswc):
    """
    .. http:get:: /works/(str:iswc)

    Function that given an id it returns the work.

    :param iswc: the id of the work.
    :type iswc: str

    :returns: The work
    :rtype: Work
    """
    return get_a_work(iswc)


@works.route('', methods=['POST'])
@protected_endpoint
@validate_schema(CreateWorkSchema())
def post_work():
    """
    .. http:post:: /works

    Function that given the work data it creates it.

    This endpoint is protected and only registered users can use it by passing
    their authentication token through the Authorization Header.

    Example::

        body = {
            "title": "Title of the song",
            "contributors": "Contributor 1|Contributor2",
            "source": "warner",
            "id": "1",
            "iswc": "T123456789"
        }
        

    :param body: the data of the work sent in the body of the request.
    :type body: dict
    :reqheader Authorization: Bearer token
    """
    data = request.get_json()
    data['user_id'] = get_user_id_from_token()
    return create_work(data)


@works.route('/<string:iswc>', methods=['PUT'])
@protected_endpoint
@validate_schema(UpdateWorkSchema())
def put_work(iswc):
    """
    .. http:put:: /works/(str:iswc)

    Function that given the iswc it updates it with the data sent in the
    body of the request.

    This endpoint is protected and only registered users can use it by passing
    their authentication token through the Authorization Header.

    Example::

        body = {
            "title": "Title of the song",
            "contributors": "Contributor 1|Contributor2",
            "source": "warner",
            "id": "1"
        }

    :param iswc: the id of the work.
    :type iswc: str
    :param body: the data of the work sent in the body of the request.
    :type body: dict
    :reqheader Authorization: Bearer token
    """
    data = request.get_json()
    data['iswc'] = iswc
    data['user_id'] = get_user_id_from_token()
    return update_work(data)


@works.route('/<string:iswc>', methods=['DELETE'])
@protected_endpoint
def delete_work(iswc):
    """
    .. http:delete:: /works/(str:iswc)

    Function that given the work iswc it deletes it.

    This endpoint is protected and only registered users can use it by passing
    their authentication token through the Authorization Header.

    :param iswc: the id of the work.
    :type iswc: str
    :reqheader Authorization: Bearer token
    """
    user_id = get_user_id_from_token()
    return delete_a_work(iswc, user_id)


@works.route('/<string:iswc>/activate', methods=['PUT'])
@protected_endpoint
def activate_work(iswc):
    """
    .. http:put:: /works/(str:iswc)

    Function that given the work iswc it activates it again.

    This endpoint is protected and only registered users can use it by passing
    their authentication token through the Authorization Header.

    :param iswc: the id of the work.
    :type iswc: str
    :reqheader Authorization: Bearer token
    """
    user_id = get_user_id_from_token()
    return activate_a_work(iswc, user_id)


@works.route('/csv-loader', methods=['POST'])
@protected_endpoint
def csv_loader():
    """
    .. http:post:: /works/csv-loader

    Function that given the work iswc it returns a csv file with the work
    metadata.

    :reqheader Authorization: Bearer token
    """
    try:
        f = request.files['file']
    except Exception:
        return jsonify('Unprocessable Entity, missing file'), 422
    if not f:
        return jsonify('Unprocessable Entity, missing csv file'), 422
    if not f.filename.endswith(".csv"):
        return jsonify('Unprocessable Entity, file must be csv'), 422
    stream = io.StringIO(f.stream.read().decode("UTF8"), newline=None)
    csv_input = csv.reader(stream)
    data = list(csv_input)
    user_id = get_user_id_from_token()
    return process_csv(data, user_id)


@works.route('/<string:iswc>/download')
def download_csv(iswc):
    """
    .. http:get:: /works/(str:iswc)/download

    Function that given the work iswc it returns a csv file with the work
    metadata.

    :param iswc: the id of the work.
    :type iswc: str
    :reqheader Authorization: Bearer token
    """
    sio = io.StringIO()
    csv_writer = csv.writer(sio)
    csv_work = get_csv_work(iswc)
    if not csv_work:
        return jsonify('Work not found'), 404
    csv_writer.writerows(csv_work)
    response = make_response(sio.getvalue())
    response.headers["Content-Disposition"] = f'attachment; filename={iswc}.csv'
    response.headers["Content-type"] = "text/csv"
    return response
