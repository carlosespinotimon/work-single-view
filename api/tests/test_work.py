#!/usr/bin/env python
# coding=utf-8

import json
import io
from datetime import datetime

import pathmagic

from base import BaseTestClass
from server.models.work_model import Work
from server.models.user_model import User


class TestWork(BaseTestClass):
    'Test Work'

    def get_token(self):
        return 'Bearer {}'.format(User.query.get(1).generate_auth_token())

    def test_create_a_work(self):
        self.create_user()
        data = {
            'title': 'Title of the song',
            'contributors': 'contributor 1|contributor 2',
            'iswc': 'T123456789',
            'source': 'entity',
            'id': '1',
        }
        res = self.tester_app.post(
            '/works',
            data=json.dumps(data),
            headers={
                'Content-Type': 'application/json',
                'Authorization': self.get_token()
            })
        self.assertEqual(res.status_code, 201)
        data = json.loads(res.get_data(as_text=True))
        expected_work = dict(eval('''{'id': '1',
                                'title': 'Title of the song',
                                'contributors': 'contributor 1|contributor 2',
                                'iswc': 'T123456789',
                                'source': 'entity--1'}'''))
        work = dict(data)
        self.assertDictEqual(work, expected_work)

    def test_get_a_work(self):
        self.create_user()
        self.create_work()
        res = self.tester_app.get(
            '/works/T123456789',
            headers={
                'Content-Type': 'application/json',
                'Authorization': self.get_token()
            })
        self.assertEqual(res.status_code, 200)
        data = json.loads(res.get_data(as_text=True))
        expected_work = dict(eval('''{'id': '1',
                                'title': 'Title of the song',
                                'contributors': 'contributor 1|contributor 2',
                                'iswc': 'T123456789',
                                'source': 'entity--1'}'''))
        work = dict(data)
        self.assertDictEqual(work, expected_work)

    def test_get_all_works(self):
        self.create_user()
        self.create_work()
        work = {
            'title': 'Title of the second song',
            'contributors': 'contributor 1|contributor 3',
            'iswc': 'T123456788',
            'source': 'entity',
            'id': '2',
            'created_by': 1,
            'last_modified_by': 1,
            'created_at': datetime.now(),
            'last_modified_at': datetime.now(),
            'is_deleted': False,
        }
        self.create_work(work)
        res = self.tester_app.get('/works/',
                                  headers={
                                      'Content-Type': 'application/json',
                                      'Authorization': self.get_token()
                                  })
        data = json.loads(res.get_data(as_text=True))
        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(data), 2)

    def test_edit_a_work(self):
        self.create_user()
        self.create_work()
        data = {
            'title': 'Title of the song 2',
            'contributors': 'contributor 1|contributor 3',
            'source': 'another entity',
            'id': '1',
        }
        res = self.tester_app.put('/works/T123456789',
                                  data=json.dumps(data),
                                  headers={
                                      'Content-Type': 'application/json',
                                      'Authorization': self.get_token()
                                  })
        data = json.loads(res.get_data(as_text=True))
        self.assertEqual(res.status_code, 200)
        work = Work.query.get(1)
        self.assertEqual(
            self._sort(work.title),
            self._sort('Title of the song|Title of the song 2')
        )
        self.assertEqual(
            self._sort(work.contributors),
            self._sort('contributor 1|contributor 2|contributor 3')
        )
        self.assertEqual(
            self._sort(work.source),
            self._sort('entity--1|another entity--1')
        )

    def test_edit_a_work_with_empty_title(self):
        self.create_user()
        self.create_work()
        data = {
            'title': '  ',
            'contributors': 'contributor 1|contributor 3',
            'source': 'another entity',
            'id': '1',
        }
        res = self.tester_app.put('/works/T123456789',
                                  data=json.dumps(data),
                                  headers={
                                      'Content-Type': 'application/json',
                                      'Authorization': self.get_token()
                                  })
        data = json.loads(res.get_data(as_text=True))
        self.assertEqual(res.status_code, 200)
        work = Work.query.get(1)
        self.assertEqual(
            self._sort(work.title),
            self._sort('Title of the song')
        )
        self.assertEqual(
            self._sort(work.contributors),
            self._sort('contributor 1|contributor 2|contributor 3')
        )
        self.assertEqual(
            self._sort(work.source),
            self._sort('entity--1|another entity--1')
        )

    def test_edit_a_work_with_empty_contributor(self):
        self.create_user()
        self.create_work()
        data = {
            'title': 'Title of the song 2',
            'contributors': ' ',
            'source': 'another entity',
            'id': '1',
        }
        res = self.tester_app.put('/works/T123456789',
                                  data=json.dumps(data),
                                  headers={
                                      'Content-Type': 'application/json',
                                      'Authorization': self.get_token()
                                  })
        data = json.loads(res.get_data(as_text=True))
        self.assertEqual(res.status_code, 200)
        work = Work.query.get(1)
        self.assertEqual(
            self._sort(work.title),
            self._sort('Title of the song|Title of the song 2')
        )
        self.assertEqual(
            self._sort(work.contributors),
            self._sort('contributor 1|contributor 2')
        )
        self.assertEqual(
            self._sort(work.source),
            self._sort('entity--1|another entity--1')
        )

    def test_edit_a_work_with_empty_source(self):
        self.create_user()
        self.create_work()
        data = {
            'title': 'Title of the song 2',
            'contributors': 'contributor 1|contributor 3',
            'source': ' ',
            'id': '1',
        }
        res = self.tester_app.put('/works/T123456789',
                                  data=json.dumps(data),
                                  headers={
                                      'Content-Type': 'application/json',
                                      'Authorization': self.get_token()
                                  })
        data = json.loads(res.get_data(as_text=True))
        self.assertEqual(res.status_code, 200)
        work = Work.query.get(1)
        self.assertEqual(
            self._sort(work.title),
            self._sort('Title of the song|Title of the song 2')
        )
        self.assertEqual(
            self._sort(work.contributors),
            self._sort('contributor 1|contributor 2|contributor 3')
        )
        self.assertEqual(
            self._sort(work.source),
            self._sort('entity--1| --1')
        )

    def test_delete_a_work(self):
        self.create_user()
        self.create_work()
        res = self.tester_app.delete('/works/T123456789',
                                     headers={
                                         'Content-Type': 'application/json',
                                         'Authorization': self.get_token()
                                     })
        self.assertEqual(res.status_code, 200)
        work = Work.query.get(1)
        self.assertEqual(work.is_deleted, True)

    def test_activate_a_deleted_a_work(self):
        self.create_user()
        self.create_work()
        work = Work.query.get(1)
        work.is_deleted = True
        res = self.tester_app.put('/works/T123456789/activate',
                                  headers={
                                      'Content-Type': 'application/json',
                                      'Authorization': self.get_token()
                                  })
        self.assertEqual(res.status_code, 200)
        work = Work.query.get(1)
        self.assertEqual(work.is_deleted, False)

    def test_wrong_params_work_creation(self):
        self.create_user()
        data = {
            'title': 'Title of the song',
            'contributors': 'contributor 1|contributor 2',
            'iswc': 'T123456789',
            'id': '1',
        }
        res = self.tester_app.post(
            '/works',
            data=json.dumps(data),
            headers={
                'Content-Type': 'application/json',
                'Authorization': self.get_token()
            })
        self.assertEqual(res.status_code, 400)
        data = json.loads(res.get_data(as_text=True))
        expected_response = dict(
            eval('''{
                "source": [
                    "Missing data for required field."
                    ]
                }'''))
        response = dict(data)
        self.assertDictEqual(response, expected_response)

    def test_empty_iswc_work_creation(self):
        self.create_user()
        data = {
            'title': 'Title of the song',
            'contributors': 'contributor 1|contributor 2',
            'iswc': ' ',
            'source': 'entity',
            'id': '1',
        }
        res = self.tester_app.post(
            '/works',
            data=json.dumps(data),
            headers={
                'Content-Type': 'application/json',
                'Authorization': self.get_token()
            })
        self.assertEqual(res.status_code, 400)
        response = json.loads(res.get_data(as_text=True))
        expected_response = "Work iswc must not be empty"
        self.assertEqual(response, expected_response)

    def test_missing_params_work_update(self):
        self.create_user()
        self.create_work()
        data = {
            'title': 'Title of the song',
            'contributors': 'contributor 1|contributor 2',
            'id': '1',
        }
        res = self.tester_app.put('/works/T123456789',
                                  data=json.dumps(data),
                                  headers={
                                      'Content-Type': 'application/json',
                                      'Authorization': self.get_token()
                                  })
        self.assertEqual(res.status_code, 400)
        data = json.loads(res.get_data(as_text=True))
        expected_response = dict(
            eval('''{
                "source": [
                    "Missing data for required field."
                    ]
                }'''))
        response = dict(data)
        self.assertDictEqual(response, expected_response)

    def test_download_work(self):
        self.create_user()
        self.create_work()
        res = self.tester_app.get('/works/T123456789/download',
                                  headers={
                                      'Content-Type': 'application/json',
                                      'Authorization': self.get_token()
                                  })
        self.assertEqual(res.status_code, 200)
        self.assertIn('filename=T123456789.csv', str(res.headers))
        expected_data = b'title,contributors,iswc,source\r\nTitle of the song,contributor 1|contributor 2,T123456789,entity--1\r\n'
        self.assertEqual(expected_data, res.data)

    def test_csv_loader(self):
        self.create_user()
        data = {}
        csv_file = ['title,contributors,iswc,source,id\r\n']
        csv_file.append(
            'Title of the song,contributor 1|contributor 2,T123456789,entity,1\r\n')
        csv_file.append(
            'Title of the song 2,contributor 3|contributor 2,T123456789,another entity,2\r\n')
        data['file'] = (io.BytesIO(str.encode(''.join(csv_file))), 'file.csv')

        res = self.tester_app.post('/works/csv-loader',
                                   data=data,
                                   headers={
                                       'Content-Type': 'multipart/form-data',
                                       'Authorization': self.get_token()
                                   })
        self.assertEqual(res.status_code, 200)
        work = Work.query.filter_by(iswc='T123456789').first()
        self.assertEqual(
            self._sort(work.title),
            self._sort('Title of the song|Title of the song 2')
        )
        self.assertEqual(
            self._sort(work.contributors),
            self._sort('contributor 1|contributor 2|contributor 3')
        )
        self.assertEqual(
            self._sort(work.source),
            self._sort('entity--1|another entity--2')
        )

    def test_csv_loader_wrong_header(self):
        self.create_user()
        data = {}
        csv_file = ['title,contributors,iswc,source,idd\r\n']
        csv_file.append(
            'Title of the song,contributor 1|contributor 2,T123456789,entity,1\r\n')
        data['file'] = (io.BytesIO(str.encode(''.join(csv_file))), 'file.csv')

        res = self.tester_app.post('/works/csv-loader',
                                   data=data,
                                   headers={
                                       'Content-Type': 'multipart/form-data',
                                       'Authorization': self.get_token()
                                   })
        self.assertEqual(res.status_code, 422)
        self.assertEqual(res.data, b'"Unprocessable Entity, wrong header"\n')

    def test_csv_loader_with_already_stored_work(self):
        self.create_user()
        self.create_work()
        data = {}
        csv_file = ['title,contributors,iswc,source,id\r\n']
        csv_file.append(
            'Title of the song,contributor 1|contributor 4,T123456789,new entity,1\r\n')
        csv_file.append(
            'Title of the song 2,contributor 3|contributor 2,T123456789,another entity,2\r\n')
        data['file'] = (io.BytesIO(str.encode(''.join(csv_file))), 'file.csv')

        res = self.tester_app.post('/works/csv-loader',
                                   data=data,
                                   headers={
                                       'Content-Type': 'multipart/form-data',
                                       'Authorization': self.get_token()
                                   })
        self.assertEqual(res.status_code, 200)
        work = Work.query.filter_by(iswc='T123456789').first()
        self.assertEqual(
            self._sort(work.title),
            self._sort('Title of the song|Title of the song 2')
        )
        self.assertEqual(
            self._sort(work.contributors),
            self._sort(
                'contributor 1|contributor 2|contributor 3|contributor 4')
        )
        self.assertEqual(
            self._sort(work.source),
            self._sort('entity--1|another entity--2|new entity--1')
        )

    def test_csv_loader_total_works_inserted(self):
        self.create_user()
        data = {}
        csv_file = ['title,contributors,iswc,source,id\r\n']
        csv_file.append(
            'Title of the song,contributor 1|contributor 2,T123456789,entity,1\r\n')
        csv_file.append(
            'Title of the song 2,contributor 3|contributor 2,T123456790,another entity,2\r\n')
        data['file'] = (io.BytesIO(str.encode(''.join(csv_file))), 'file.csv')

        res = self.tester_app.post('/works/csv-loader',
                                   data=data,
                                   headers={
                                       'Content-Type': 'multipart/form-data',
                                       'Authorization': self.get_token()
                                   })
        self.assertEqual(res.status_code, 200)
        works = Work.query.all()
        self.assertEqual(len(works), 2)

    def _sort(self, list_):
        return sorted(list_.split("|"))
